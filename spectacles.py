import time
import requests
import os
import sys

# Define API key and IDs
api_key = os.getenv("SPECTACLES_API_KEY")
org_id = "WstGk40UEDhElCWTYiZ9"
suite_id = "p0t0ZZE7iQuorvC4MJ3E"
project_id = "JDmbNOfdWj0B50B6zqE6"
commit = os.getenv("BITBUCKET_COMMIT")

# Set the API key in header
headers = {"Authorization": f"Bearer {api_key}", "Content-Type": "application/json"}

# Create a run
create_run_url = "https://app.spectacles.dev/api/v1/runs"
payload = {
    "org_id": org_id,
    "suite_id": suite_id,
    "project_id": project_id,
    "commit": commit,
}
create_run_response = requests.post(url=create_run_url, headers=headers, json=payload)
create_run_response.raise_for_status()
run_id = create_run_response.json()["run_id"]

run_status = "queued"
run_url = (
    f"https://app.spectacles.dev/api/v1/org/{org_id}/proj/{project_id}/run/{run_id}"
)

while run_status not in ["cancelled", "error", "passed", "failed"]:

    # Naively wait for 5 seconds to check
    time.sleep(5)

    # Get the run's results
    run_response = requests.get(url=run_url, headers=headers)

    run_status = run_response.json()["status"]

print(run_response.json()["url"])

if run_status != "passed":
    sys.exit(100)
